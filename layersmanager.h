#ifndef LAYERSMANAGER_H
#define LAYERSMANAGER_H

#include "layereditor.h"
#include "serializableclass.h"

#include <QObject>
#include <QString>
#include <layer.h>
#include <QVector>
#include <QLabel>
#include <QGraphicsView>

class activeProject;

class layersManager : public SerializableClass
{
    Q_OBJECT
public:
    explicit layersManager(QObject *parent, activeProject *project);
    layersManager(layersManager&) = default;

    void draw();

    layer * getActive();
    void setActive(int index);
    layer * addLayer(int index);
    void restoreLayer(int index, layer * _layer);
    layer * removeLayer(int index);
    void removeLayer(int index, layer * _layer);

    QGraphicsScene* mScene;
    layerEditor mEditor;
signals:

public slots:
    void update();

private:
    activeProject * mProject;
    QVector<layer *> mActiveLayers;
    qint32 mActiveIndex;

    friend QDataStream & operator<<(QDataStream &out, layersManager& manager);
    friend QDataStream & operator>>(QDataStream &in, layersManager& manager);
};

QDataStream & operator<<(QDataStream &out, layersManager& manager);
QDataStream & operator>>(QDataStream &in, layersManager& manager);

#endif // LAYERSMANAGER_H
