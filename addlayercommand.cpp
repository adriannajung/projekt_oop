#include "addlayercommand.h"
#include "layer.h"

addLayerCommand::addLayerCommand(layer * _layer, int index) :
    mLayer(_layer),
    mIndex(index)
{
}

void addLayerCommand::redo()
{
    mLayer->restore(mIndex);
}

void addLayerCommand::undo()
{
    mLayer->remove(mIndex);
}
