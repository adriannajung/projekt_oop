#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QWidget>
#include <Qt>
#include <QtDebug>

class ClickableLabel : public QLabel {
    Q_OBJECT

public:
    explicit ClickableLabel(int index, QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~ClickableLabel();

signals:
    void clicked(int);

protected:
    void mousePressEvent(QMouseEvent* event);

private:
    int labelIndex;
};

#endif // CLICKABLELABEL_H
