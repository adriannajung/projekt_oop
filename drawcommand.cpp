#include "drawcommand.h"
#include "layer.h"

drawCommand::drawCommand(layer * _layer, QString brushType, int brushSize, QColor color) :
    mLayer(_layer),
    mCopy(_layer->mImage.copy()),
    mBrushType(brushType),
    mBrushSize(brushSize),
    mColor(color)
{
}

void drawCommand::addPoint(QPoint point)
{
    mPoints.append(point);
    redo();
}

void drawCommand::redo()
{
    mLayer->mImage = mCopy;

    QPainter paint;
    paint.begin(&mLayer->mImage);

    QPen pen(mColor);
    paint.setPen(pen);

    QBrush brush(mColor);
    paint.setBrush(brush);

    if (mBrushType == "Circle"){

        for (auto & point : mPoints)
            paint.drawEllipse(point.x() - mBrushSize / 2.,point.y() - mBrushSize / 2., mBrushSize, mBrushSize);

    } else {

        for (auto & point : mPoints)
            paint.drawRect(point.x() - mBrushSize / 2.,point.y() - mBrushSize / 2., mBrushSize, mBrushSize);
    }

    paint.end();
    mLayer->refresh();
}

void drawCommand::undo()
{
    mLayer->mImage = mCopy;
}

