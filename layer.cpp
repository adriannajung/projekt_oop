#include "layer.h"
#include "activeproject.h"

#include <QPainter>
#include <QPoint>

layer::layer(QObject *parent, activeProject * project, QGraphicsScene * scene) :
    QObject(parent),
    mProject(project),
    mScene(scene),
    mIsOnScene(0),
    mIsVisible(1),
    mName(),
    mHasChanged(0)
{

}

void layer::loadImage(QString fileName)
{
    mImage.load(fileName);

    if (!mImage.isNull()){
        mImage = mImage.copy(mProject->mImageSize);
        mItem.setPixmap(QPixmap::fromImage(mImage));
    }
}

void layer::draw()
{
    if (mImage.isNull()){
        mImage = QImage(mProject->mImageSize.width(), mProject->mImageSize.height(), QImage::Format_ARGB32);
        mImage.fill(QColor(0, 0, 0, 0));
        mHasChanged = 1;
    }

    if (mHasChanged)
        mItem.setPixmap(QPixmap::fromImage(mImage));

    if (mIsVisible && !mIsOnScene){
        mScene->addItem(&mItem);
        mIsOnScene = 1;
    } else if (!mIsVisible && mIsOnScene){
        mScene->removeItem(&mItem);
        mIsOnScene = 0;
    }
}

void layer::setVisibility(bool to)
{
    mIsVisible = to;
}

void layer::remove(int index)
{
    if (mIsOnScene){
        mScene->removeItem(&mItem);
        mIsOnScene = 0;
    }

    mProject->mLayersManager.removeLayer(index, this);
}

void layer::restore(int index)
{
    mProject->mLayersManager.restoreLayer(index, this);
}

void layer::refresh()
{
    mHasChanged = true;
}

QDataStream &operator<<(QDataStream &out, const layer &_layer)
{
    out << _layer.mImage << _layer.mIsVisible;

    return out;
}

QDataStream &operator>>(QDataStream &in, layer &_layer)
{
    in >> _layer.mImage >> _layer.mIsVisible;

    return in;
}

layer &operator<<(layer &layerLeft, layer &layerRight)
{
    layerLeft.mImage = layerRight.mImage;
    layerLeft.mIsVisible = layerRight.mIsVisible;
    layerLeft.mIsOnScene = false;
    layerLeft.mHasChanged = true;

    return layerLeft;
}
