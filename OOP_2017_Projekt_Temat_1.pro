#-------------------------------------------------
#
# Project created by QtCreator 2018-02-02T10:19:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OOP_2017_Projekt_Temat_1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    editform.cpp \
    historyform.cpp \
    toolsform.cpp \
    activeproject.cpp \
    layer.cpp \
    layersmanager.cpp \
    layersform.cpp \
    addlayercommand.cpp \
    removelayercommand.cpp \
    layereditor.cpp \
    drawcommand.cpp \
    clickablelabel.cpp \
    serializableclass.cpp \
    changecontrastcommand.cpp

HEADERS += \
        mainwindow.h \
    editform.h \
    historyform.h \
    toolsform.h \
    activeproject.h \
    layer.h \
    layersmanager.h \
    layersform.h \
    addlayercommand.h \
    removelayercommand.h \
    layereditor.h \
    drawcommand.h \
    clickablelabel.h \
    serializableclass.h \
    changecontrastcommand.h

FORMS += \
        mainwindow.ui \
    editform.ui \
    historyform.ui \
    toolsform.ui \
    layersform.ui

RESOURCES += \
    layer.qrc
