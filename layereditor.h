#ifndef LAYEREDITOR_H
#define LAYEREDITOR_H



#include "drawcommand.h"

#include <QMouseEvent>
#include <QObject>

class activeProject;

class layerEditor : public QObject
{
    Q_OBJECT
public:
    explicit layerEditor(QObject *parent, activeProject * project);
    void mouseDrawEvent(QMouseEvent* event);

    QString mBrushType;
    int mBrushSize;

    double mRed;
    double mGreen;
    double mBlue;
    double mAlpha;
signals:

public slots:

private:
    activeProject * mProject;
    drawCommand * mCurrentCommand;
};

#endif // LAYEREDITOR_H
